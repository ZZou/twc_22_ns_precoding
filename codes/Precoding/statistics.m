%% statistics 
close all
clear all
%%
c = load ('h_d10_v120.mat');
par = load('par_d10_v120.mat');

%%
Nu = par.Nu;
Nt = par.Nt;
Bw = par.Bw;
Ns = par.Ns;
fs = par.fs;
dt = par.dt;
save = 1;
N_snap = 4000;
fc = par.fc;
p = par.p;
h = c.h;
T_diff = round(dt*Bw);
N_ofdm = 10;
N_cp = Ns/4;
T = (Ns+N_cp)*N_ofdm; % 10 ofdm symbols = 0.04 ms
% Stat_T = 100*T;
%% interpolate
% hc = zeros(Nu,Nt,N_carrier,T);
% for u1 = 1:Nu
%    for u2 = 1:Nt
%       for t1 = 1:N_carrier
%          for t2 = 1:Stat_T
%             t_temp = round(t2/T_diff)+1;
%             hc(u1,u2,t1,t2) = h(u1,u2,t1,t_temp);
%          end
%       end
%    end
% end
%%
t1 = round(1e-3/dt);
plt_h_t = abs(squeeze(h(1,1,:,:)));
acf = [];
lags = [];
t = [1:t1];
for i = 1:length(t)
    [acf(:,i),lags(:,i)] = autocorr(plt_h_t(:,t(i)));
end
acf = permute(acf,[2,1]);
[xcor, ycor] = meshgrid(1:size(acf,1), 1:size(acf,2));
figure;
% plot3(ycor, xcor/2, acf,'-o');
mesh(ycor, xcor/t1, acf');
% legend('1ms','2ms','3ms','4ms','5ms','6ms','7ms','8ms','9ms','10ms');
% plot(lags, acf(1,:),lags, acf(2,:),lags, acf(3,:),lags, acf(4,:),lags, acf(5,:));
xlabel('Lags');
ylabel('Time (ms)');
zlabel('ACF');
view(35,35);
set(gca,'fontsize',16);
filename = 'Acf';
savefile = 1;
[h, wd, ht] = tightfig();
if savefile == 1
    name1 = append(filename, '.fig');
    name2 = append(filename, '.pdf');
    saveas(gca, name1);
    exportgraphics(gca, name2);
end
%% boxplot
figure;
boxplot(acf);
xlabel('Lags');
ylabel('Average ACF');
set(gca,'fontsize',16);
filename = 'boxplot';
savefile = 1;
[h, wd, ht] = tightfig();
if savefile == 1
    name1 = append(filename, '.fig');
    name2 = append(filename, '.pdf');
    saveas(gca, name1);
    exportgraphics(gca, name2);
end